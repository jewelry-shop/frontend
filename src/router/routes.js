// import { mapGetters } from 'vuex'

// ...mapGetters({
// 	ifAuth: 'auth/isAuthenticated'
// })

// const ifNotAuth = (to, from, next) => {
// 	if (!ifAuth) {
// 		next()
// 	}
// }
const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
    	{
    		path: "",
    		component: () => import("pages/Index.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/cart",
    		component: () => import("pages/cart.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/sale",
    		component: () => import("pages/sale.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/cab",
    		component: () => import("pages/cab.vue"),
    		meta: { requiresAuth: true }
    	},
    	{
    		path: "/about",
    		component: () => import("pages/about.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/favourite",
    		component: () => import("pages/favourite.vue"),
    		meta: { requiresAuth: true }
    	},
    	{
    		path: "/catalog/:categories",
    		component: () => import("pages/catalog.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/contacts",
    		component: () => import("pages/contacts.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/search",
    		component: () => import("pages/search.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/new",
    		component: () => import("pages/new.vue"),
    		meta: { guest: true }
    	},
    ]
  },
  {
  	path: "/",
    component: () => import("layouts/emptyLayout.vue"),
    children: [
    	{
    		path: "/admin",
    		component: () => import("pages/admin.vue"),
    		meta: { guest: true }
    	},
    	{
    		path: "/login",
    		component: () => import("pages/login.vue"),
    		meta: { requiresAuth: true, is_admin: true }
    	},
    	{
    		path: "/signup",
    		component: () => import("pages/signup.vue"),
    		meta: { guest: true }
    	}
    ]
  }
];

// routes.beforeEach((to, from, next) => {
// 	if(to.matched.some(record => record.meta.requiresAuth)) {
// 		if (localStorage.getItem('user-token') == null) {
// 			next({
// 				path: '/login',
// 				params: { nextUrl: to.fullPath }
// 			})
// 		} else {
// 			let user = JSON.parse(localStorage.getItem('user-token'))
// 			if (to.matched.some(record => record.meta.is_admin)) {
// 				if(user.is_admin == 1){
// 					next()
// 				}
// 				else{
// 					next({ path: '/login' })
// 				}
// 			}else {
// 				next()
// 			}
// 		}
// 	} else if(to.matched.some(record => record.meta.guest)) {
// 		if(localStorage.getItem('user-token') == null){
// 			next()
// 		}
// 		else{
// 			next({ path: '/login' })
// 		}
// 	}else {
// 		next()
// 	}
// })

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
