import API_URL from '../../boot/api'

const state = {
	fproducts: 1
}

const mutations = {
	FILTER_PRODUCTS (state, data) {
		state.fproducts = data
	}
}

const actions = {
	filter_products (context, ...data) {
		window.console.log(data)
		return new Promise((resolve, reject) => {
			API_URL.put('filter', ...data)
				.then((response) => {
					window.console.log(data)
					context.commit('FILTER_PRODUCTS', response.data)
					window.console.log(response)
					resolve(response)
				})
				.catch((error) => {
					reject(error)
				})
		})
	}
}

const getters = {
	get_filtered: state => state.fproducts
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
