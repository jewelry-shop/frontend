import axios from 'axios'
import API_URL from '../../boot/api'

const state = {
	products: 1
}

const mutations = {
	SET_PRODUCTS (state, data) {
		state.products = data
	}
}

const actions = {
	getProducts (context) {
		return new Promise((resolve, reject) => {
			axios.get(API_URL + 'products/')
			    .then(response => {
			      	context.commit('SET_PRODUCTS', response.data.data)
			       	resolve(response)
				})
			    .catch(error => {
		        	reject(error)
		        })
		})
	}
}

const getters = {
	products: (state) => {
    	return state.products
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
