import API_URL from '../../boot/api'

const state = {
	token: localStorage.getItem('user-token') || ''
}

const mutations = {
	AUTH_SUCCESS (state, data) {
		state.token = data.token
	},
	AUTH_LOGOUT (state) {
		state.token = ''
	}
}

const actions = {
	// login_action(context){
	// 	axios.post('url/login',
	// 		{email: this.email, password: this.password},
	// 		{headers: {'X-Requested_With': 'XMLHttpRequest'}})
	// 		.then(
	// 			(response) => {
	// 				const token = response.data.token;
	// 				const base64Url = token.split('.')[1];
	// 				const base64 = base64Url.replace('-', '+').replace('_', '/');
	// 				localStorage.setItem('token', token);
	// 			})
	// 		.catch(
	// 			(error) => console)
	// }
	login ({ commit }, data) {
		return new Promise((resolve, reject) => {
			API_URL.post('login', data)
				.then((response) => {
					const token = response.data.access_token
					commit('AUTH_SUCCESS', token)
					localStorage.setItem('user-token', token)
					API_URL.defaults.headers.common['Authorization'] = 'Bearer' + token
					window.console.log(response)
					resolve(response)
				})
				.catch((error) => {
					reject(error)
				})
		})
	},
	signup ({ commit }, data) {
		return new Promise((resolve, reject) => {
			API_URL.post('register', data)
				.then((response) => {
					const token = response.data.access_token
					commit('AUTH_SUCCESS', token)
					localStorage.setItem('user-token', token)
					API_URL.defaults.headers.common['Authorization'] = 'Bearer' + token
					window.console.log(response)
					resolve(response)
				})
				.catch((error) => {
					reject(error)
				})
		})
	}
}

const getters = {
	isAuthenticated: state => !!state.token
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
