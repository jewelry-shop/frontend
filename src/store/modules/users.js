import axios from 'axios'
import API_URL from '../../boot/api'

const state = {
	users: 1
}

const mutations = {
	SET_USERS (state, data) {
		state.users = data
	}
}

const actions = {
	getUsers (context) {
		return new Promise((resolve, reject) => {
			axios.get(API_URL + '/users')
			    .then(response => {
			      	context.commit('SET_USERS', response.data)
			       	resolve(response)
				})
			    .catch(error => {
		        	reject(error)
		        })
		})
	}
}

const getters = {
	usersInfo: (state) => {
    	return state.users
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
